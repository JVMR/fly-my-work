import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'flight.dart';

final String flightTable = "flightTable";
final String idColumn = "idColumn";
final String titleColumn = "titleColumn";
final String localColumn = "localColumn";
final String dataColumn = "dataColumn";
final String statusColumn = "statusColumn";

class FlightHelper {
  static final FlightHelper _instance = FlightHelper.internal();

  factory FlightHelper() => _instance;

  FlightHelper.internal();

  Database _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    } else {
      _db = await initDb();
      return _db;
    }
  }

  Future<Database> initDb() async {
    final databasesPath = await getDatabasesPath();
    final path = join(databasesPath, "flights.db");

    return await openDatabase(path, version: 1,
        onCreate: (Database db, int newerVersion) async {
      await db.execute(
          "CREATE TABLE $flightTable($idColumn INTEGER PRIMARY KEY, $titleColumn TEXT, $localColumn TEXT,"
          "$dataColumn TEXT, $statusColumn TEXT)");
    });
  }

  Future<Flight> saveFlight(Flight flight) async {
    Database dbContact = await db;
    flight.id = await dbContact.insert(flightTable, flight.toMap());
    return flight;
  }

  Future<Flight> getFlight(int id) async {
    Database dbContact = await db;
    List<Map> maps = await dbContact.query(flightTable,
        columns: [idColumn, titleColumn, localColumn, dataColumn, statusColumn],
        where: "$idColumn = ?",
        whereArgs: [id]);
    if (maps.length > 0) {
      return Flight.fromMap(maps.first);
    } else {
      return null;
    }
  }

  Future<int> deleteFlight(int id) async {
    Database dbFlight = await db;
    return await dbFlight
        .delete(flightTable, where: "$idColumn = ?", whereArgs: [id]);
  }

  Future<int> updateFlight(Flight flight) async {
    Database dbFlight = await db;
    return await dbFlight.update(flightTable, flight.toMap(),
        where: "$idColumn = ?", whereArgs: [flight.id]);
  }

  Future<List> getAllFlights() async {
    Database dbFlight = await db;
    List listMap = await dbFlight.rawQuery("SELECT * FROM $flightTable");
    List<Flight> listFlights = List();
    for (Map m in listMap) {
      listFlights.add(Flight.fromMap(m));
    }
    return listFlights;
  }

  Future<int> getNumber() async {
    Database dbFlight = await db;
    return Sqflite.firstIntValue(
        await dbFlight.rawQuery("SELECT COUNT(*) FROM $flightTable"));
  }

  Future close() async {
    Database dbFlight = await db;
    dbFlight.close();
  }
}
