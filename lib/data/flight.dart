import 'flight_helper.dart';

class Flight {
  int id;
  String title;
  String local;
  String data;
  String status;
  //String json; //Referência ao arquivo Json

  Flight();
  Flight.fromMap(Map map) {
    id = map[idColumn];
    title = map[titleColumn];
    local = map[localColumn];
    data = map[dataColumn];
    status = map[statusColumn];
  }

  Map toMap() {
    Map<String, dynamic> map = {
      titleColumn: title,
      localColumn: local,
      dataColumn: data,
      statusColumn: status
    };
    if (id != null) {
      map[idColumn] = id;
    }
    return map;
  }

  @override
  String toString() {
    return "Contact(id: $id, title: $title, local: $local, data: $data, status: $status)";
  }
}
