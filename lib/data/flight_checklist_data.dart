import 'dart:convert';
import 'dart:io';

import 'package:path_provider/path_provider.dart';

class FlightChecklistData {
  int id;
  List _flightChecklist = [];

  FlightChecklistData(this.id);

  void create() {
    createChecklist();
  }

  List get() => _flightChecklist;

  Map<String, dynamic> addMap(
      //Função utilizada internamente para criar a checklist
      String title,
      bool value,
      String category,
      bool canRemove) {
    Map<String, dynamic> newQuestion = Map();
    newQuestion["title"] = title;
    newQuestion["value"] = value;
    newQuestion["category"] = category;
    newQuestion["canRemove"] = canRemove;
    return newQuestion;
  }

  Future<File> getFile() async {
    final directory = await getApplicationDocumentsDirectory();
    return File("${directory.path}/data$id.json");
  }

  /*Future<File> saveData() async {
    String data = json.encode(_flightChecklist);

    final file = await getFile();
    return file.writeAsString(data);
  }*/

  Future<File> saveData(List flightChecklistModified) async {
    _flightChecklist = flightChecklistModified;
    String data = json.encode(_flightChecklist);

    final file = await getFile();
    return file.writeAsString(data);
  }

  Future<String> readData(int id) async {
    try {
      final file = await getFile();
      return file.readAsString();
    } catch (e) {
      return null;
    }
  }

  void createChecklist() {
    Map<String, dynamic> _flightCategory1 = {};
    Map<String, dynamic> _flightCategory2 = {};
    Map<String, dynamic> _flightCategory3 = {};
    Map<String, dynamic> _flightCategory4 = {};
    Map<String, dynamic> _flightCategory5 = {};
    Map<String, dynamic> _flightCategory6 = {};
    Map<String, dynamic> _flightCategory7 = {};
    Map<String, dynamic> _flightCategory8 = {};
    Map<String, dynamic> _flightCategory9 = {};
    Map<String, dynamic> _flightCategory10 = {};
    Map<String, dynamic> _flightCategory11 = {};
    Map<String, dynamic> _flightCategory12 = {};
    Map<String, dynamic> _flightCategory13 = {};
    Map<String, dynamic> _flightCategory14 = {};
    Map<String, dynamic> _flightCategory15 = {};
    Map<String, dynamic> _flightCategory16 = {};
    Map<String, dynamic> _flightCategory17 = {};
    Map<String, dynamic> _flightCategory18 = {};
    Map<String, dynamic> _flightCategory19 = {};

    _flightCategory1["title"] = "Legislação Aplicável";
    _flightCategory1["onComplete"] = false;
    _flightCategory1["editable"] = false;
    _flightCategory1["description"] = "Regularização de equipamento e missões";
    _flightCategory1["numQuestions"] = 5;
    _flightCategory1["questions"] = {
      "1": "Anatel",
      "value1": false,
      "observation1": "",
      "2": "Anac",
      "value2": false,
      "observation2": "",
      "3": "Decea",
      "value3": false,
      "observation3": "",
      "4": "MD",
      "value4": false,
      "observation4": "",
      "5": "Comando Polícia Militar",
      "value5": false,
      "observation5": "",
    };
    _flightChecklist.add(_flightCategory1);

    _flightCategory2["title"] = "Aeronave Remotamente Pilotada (RPA)";
    _flightCategory2["onComplete"] = false;
    _flightCategory2["editable"] = true;
    _flightCategory2["description"] =
        "Antes de sair para o campo,\nverifique se o RPA está dentro do case e seus acessórios.";
    _flightCategory2["numQuestions"] = 13;
    _flightCategory2["questions"] = {
      "1": "RPA",
      "value1": false,
      "observation1": "Dentro do case ?",
      "2": "RPA: sinais de danos no casco ou fios soltos?",
      "value2": false,
      "observation2": "",
      "3": "Manual do equipamento",
      "value3": false,
      "observation3": "",
      "4": "Avaliação de risco operacional",
      "value4": false,
      "observation4": "",
      "5": "Kit ferramentas em caso de manutenção em campo",
      "value5": false,
      "observation5": "",
      "6": "Checar atualizações de firmware e softwares",
      "value6": false,
      "observation6": "",
      "7": "SARPAS (Decea)",
      "value7": false,
      "observation7": "",
      "8": "Seguro RETA da RPA",
      "value8": false,
      "observation8": "",
      "9": "Seguro de casco da RPA",
      "value9": false,
      "observation9": "",
      "10": "Calibração: RC, IMU e Gimble",
      "value10": false,
      "observation10": "",
      "11": "No Fly Zone",
      "value11": false,
      "observation11": "(pistas de aeroportos e aeródromos)",
      "12": "Condições meteorológicas",
      "value12": false,
      "observation12": "(incluindo tempestade solar - KP-index)",
      "13": "Locais para pouso de emergência",
      "value13": false,
      "observation13": "",
    };
    _flightChecklist.add(_flightCategory2);

    _flightCategory3["title"] = "Decolagem/Pouso\nLançamento/Recuperação";
    _flightCategory3["onComplete"] = false;
    _flightCategory3["editable"] = true;
    _flightCategory3["description"] =
        "Observação: estes equipamentos de lançamento ou pouso são aplicados\napenas a alguns modelos de ARPs.";
    _flightCategory3["numQuestions"] = 19;
    _flightCategory3["questions"] = {
      "1": "Catapulta",
      "value1": false,
      "observation1": "",
      "2": "Paraquedas",
      "value2": false,
      "observation2": "",
      "3": "Rede, gancho ou colchão de ar",
      "value3": false,
      "observation3": "",
      "4": "Plataforma de decolagem e pouso",
      "value4": false,
      "observation4": "",
      "5": "Obstáculos decolagem/pouso",
      "value5": false,
      "observation5": "(torres alta tensão, silo, água, etc)",
      "6": "Fontes de interferência eletromagnética",
      "value6": false,
      "observation6": "",
      "7": "Sinal de GPS/GNSS",
      "value7": false,
      "observation7": "",
      "8": "Presença apenas da equipe de trabalho no local",
      "value8": false,
      "observation8": "",
      "9": "RC x ARP: sequência de funcionamento",
      "value9": false,
      "observation9": "",
      "10": "Multirrotor: frente da ARP sentido contrário ao piloto",
      "value10": false,
      "observation10": "",
      "11": "Lentes da câmera sem resíduos",
      "value11": false,
      "observation11": "",
      "12": "Multirrotor: hélices fixadas corretamente",
      "value12": false,
      "observation12": "",
      "13": "Phantom: salvar home point",
      "value13": false,
      "observation13": "",
      "14": "Phantom: protetor do gimble",
      "value14": false,
      "observation14": "",
      "15": "Phantom: Configurações DJI Go e Aplicativo de voo",
      "value15": false,
      "observation15": "",
      "16": "Phantom: altura configurada de pouso x obstáculos",
      "value16": false,
      "observation16": "",
      "17": "Phantom: RC no modo 'P' ",
      "value17": false,
      "observation17": "",
      "18": "Phantom: calibração compass",
      "value18": false,
      "observation18": "",
      "19": "Phantom e Mavic: calibração de sensores anticolisão",
      "value19": false,
      "observation19": "",
    };
    _flightChecklist.add(_flightCategory3);

    _flightCategory4["title"] = "Acessórios";
    _flightCategory4["onComplete"] = false;
    _flightCategory4["editable"] = true;
    _flightCategory4["description"] =
        "Lembre-se de todos os acessórios que você deve levar para fazer seu levantamento de campo.";
    _flightCategory4["numQuestions"] = 15;
    _flightCategory4["questions"] = {
      "1": "Cartão de Memória",
      "value1": false,
      "observation1": "Espaço suficiente?",
      "2": "Bateria (Para o ARP)",
      "value2": false,
      "observation2": "Carregada?",
      "3": "Bateria reserva (Para o ARP)",
      "value3": false,
      "observation3": "Carregada?",
      "4": "Hélices",
      "value4": false,
      "observation4": "Balanceadas?",
      "5": "Rádio-controle",
      "value5": false,
      "observation5": "",
      "6": "Bateria (Para o rádio-controle)",
      "value6": false,
      "observation6": "Carregada?",
      "7": "Correia para o pescoço",
      "value7": false,
      "observation7": "",
      "8": "Carregador/balanceador de baterias",
      "value8": false,
      "observation8": "",
      "9": "Inversor de 12v para 110/220v",
      "value9": false,
      "observation9": "",
      "10": "Voltímetro",
      "value10": false,
      "observation10": "",
      "11": "Cabo USB",
      "value11": false,
      "observation11": "",
      "12": "HD externo para backup dos dados",
      "value12": false,
      "observation12": "",
      "13": "Celular ou Tablet: bateria e aplicativos",
      "value13": false,
      "observation13": "",
      "14": "Defletor de sinal (antena RC)",
      "value14": false,
      "observation14": "",
      "15": "Protetor solar para monitor",
      "value15": false,
      "observation15": "",
    };
    _flightChecklist.add(_flightCategory4);

    _flightCategory5["title"] = "Planejamento de Voo";
    _flightCategory5["onComplete"] = false;
    _flightCategory5["editable"] = true;
    _flightCategory5["description"] =
        "De nada adianta lembrar-se de todos os itens, mas não conseguir fazer o levantamento por falta de um planejamento adequado. Confira o que não dá para esquecer.";
    _flightCategory5["numQuestions"] = 6;
    _flightCategory5["questions"] = {
      "1": "Notam (ANAC)",
      "value1": false,
      "observation1": "",
      "2": "Mapa/Croqui de área",
      "value2": false,
      "observation2": "",
      "3": "Plano de voo detalhado",
      "value3": false,
      "observation3": "",
      "4": "Verificar condições meteorológicas em campo",
      "value4": false,
      "observation4": "",
      "5": "Checar os níveis de raios solares (KP-index) em campo",
      "value5": false,
      "observation5": "",
      "6": "Caderneta de campo",
      "value6": false,
      "observation6": "",
    };
    _flightChecklist.add(_flightCategory5);

    _flightCategory6["title"] = "Ferramentas";
    _flightCategory6["onComplete"] = false;
    _flightCategory6["editable"] = true;
    _flightCategory6["description"] =
        "Ter em mãos ferramentas pra fazer um reparo no próprio local do levantamento pode fazer diferença e evitar mais uma ida a campo.";
    _flightCategory6["numQuestions"] = 16;
    _flightCategory6["questions"] = {
      "1": "Chaves para montar/desmontar",
      "value1": false,
      "observation1": "Philips, de fenda, allen, etc",
      "2": "Fita isolante/3m dupla face",
      "value2": false,
      "observation2": "",
      "3": "Hélices reservas balanceadas",
      "value3": false,
      "observation3": "",
      "4": "Motores reservas",
      "value4": false,
      "observation4": "",
      "5": "Peças reservas",
      "value5": false,
      "observation5": "(diversas)",
      "6": "Bateria",
      "value6": false,
      "observation6": "(Para o rádio-controle)",
      "7": "Alicates",
      "value7": false,
      "observation7": "",
      "8": "Mini multímetro",
      "value8": false,
      "observation8": "(medição de tensão/corrente)",
      "9": "Paquímetro, balança, ferro para soldar, estanho, sugador de solda",
      "value9": false,
      "observation9": "",
      "10": "Lupa",
      "value10": false,
      "observation10": "",
      "11": "Trava-rosca",
      "value11": false,
      "observation11": "",
      "12": "Conectores e plugs reservas",
      "value12": false,
      "observation12": "",
      "13": "Cabos reservas",
      "value13": false,
      "observation13": "",
      "14": "Parafusos e porcas reservas",
      "value14": false,
      "observation14": "",
      "15": "Adesivos para consertos de emergência",
      "value15": false,
      "observation15": "",
      "16": "Resina EPOX",
      "value16": false,
      "observation16": "",
    };
    _flightChecklist.add(_flightCategory6);

    _flightCategory7["title"] = "Água";
    _flightCategory7["onComplete"] = false;
    _flightCategory7["editable"] = true;
    _flightCategory7["description"] =
        "A água é um item importante a ser lembrado antes de sair a campo para um trabalho com ARPs.\nLembre-se de levar muita água, pois você pode precisar usá-la para limpar um equipamento, além da hidratação da equipe.";
    _flightCategory7["numQuestions"] = 1;
    _flightCategory7["questions"] = {
      "1": "Água",
      "value1": false,
      "observation1": "",
    };
    _flightChecklist.add(_flightCategory7);

    _flightCategory8["title"] = "Veículos";
    _flightCategory8["onComplete"] = false;
    _flightCategory8["editable"] = true;
    _flightCategory8["description"] =
        "A forma de locomoção é muito importante, pois para chegar ao local é necessário que o(s) seu(s) veículo(s) tenha(m) condições para ir e voltar, sem maiores riscos. Por isso, em alguns casos é indicado o uso de veículos com tração nas quatro rodas, e em todos os casos, estarem equipados com ferramentas de manutenção.";
    _flightCategory8["numQuestions"] = 3;
    _flightCategory8["questions"] = {
      "1": "Veículo para locomoção/Equipamentos de Manutenção",
      "value1": false,
      "observation1": "Dentro do case?",
      "2": "Seguro do Veículo",
      "value2": false,
      "observation2": "",
      "3": "Combustível",
      "value3": false,
      "observation3": "",
    };
    _flightChecklist.add(_flightCategory8);

    _flightCategory9["title"] = "Veículos";
    _flightCategory9["onComplete"] = false;
    _flightCategory9["editable"] = true;
    _flightCategory9["description"] =
        "Os objetos que você irá transportar precisam estar bem protegidos e sempre à mão, portanto é importante levar sempre uma mochila maior com os itens mais pesados, troca de roupa, cadernos, etc., que podem ser deixados no carro, e uma mochila menor onde ficarão os itens que precisam estar sempre á mão, como câmera, GPS, etc..";
    _flightCategory9["numQuestions"] = 3;
    _flightCategory9["questions"] = {
      "1": "Mochila/Bolsa grande",
      "value1": false,
      "observation1": "",
      "2": "Mochila/ Bolsa pequena",
      "value2": false,
      "observation2": "",
      "3": "Sacos plásticos",
      "value3": false,
      "observation3": "",
    };
    _flightChecklist.add(_flightCategory9);

    _flightCategory10["title"] = "EPI'S";
    _flightCategory10["onComplete"] = false;
    _flightCategory10["editable"] = true;
    _flightCategory10["description"] =
        "Em todo o trabalho de campo são necessários Equipamentos de Proteção Individual (EPIs) para que não ocorram acidentes durante o levantamento.";
    _flightCategory10["numQuestions"] = 3;
    _flightCategory10["questions"] = {
      "1": "Capacete",
      "value1": false,
      "observation1": "",
      "2": "Óculos de sol",
      "value2": false,
      "observation2": "",
      "3": "Cordas",
      "value3": false,
      "observation3": "",
    };
    _flightChecklist.add(_flightCategory10);

    _flightCategory11["title"] = "Farmácia";
    _flightCategory11["onComplete"] = false;
    _flightCategory11["editable"] = true;
    _flightCategory11["description"] =
        "Os itens da farmácia e primeiros socorros também são indispensáveis. No caso de haver algum imprevisto, ter esses itens em mão pode ser de grande ajuda.";
    _flightCategory11["numQuestions"] = 2;
    _flightCategory11["questions"] = {
      "1": "Protetor solar/Repelente",
      "value1": false,
      "observation1": "",
      "2": "Kit Primeiros socorros",
      "value2": false,
      "observation2": "",
    };
    _flightChecklist.add(_flightCategory11);

    _flightCategory12["title"] = "Vestuário";
    _flightCategory12["onComplete"] = false;
    _flightCategory12["editable"] = true;
    _flightCategory12["description"] =
        "O vestuário é também parte importante a ser cuidada. Tanto a roupa quanto o calçado a serem utilizados podem fazer grande diferença na comodidade e proteção durante a saída de campo.";
    _flightCategory12["numQuestions"] = 3;
    _flightCategory12["questions"] = {
      "1": "Camiseta longa/Colete",
      "value1": false,
      "observation1": "",
      "2": "Chapéu/boné",
      "value2": false,
      "observation2": "",
      "3": "Bota/Galocha/Tênis",
      "value3": false,
      "observation3": "",
    };
    _flightChecklist.add(_flightCategory12);

    _flightCategory13["title"] = "Equipamentos Auxiliares";
    _flightCategory13["onComplete"] = false;
    _flightCategory13["editable"] = true;
    _flightCategory13["description"] =
        "Não se esqueça de equipamentos auxiliares que vão lhe ajudar no processo de trabalho em campo.";
    _flightCategory13["numQuestions"] = 9;
    _flightCategory13["questions"] = {
      "1": "Binóculo",
      "value1": false,
      "observation1": "",
      "2": "Facão/Canivete",
      "value2": false,
      "observation2": "",
      "3": "GPS de Navegação e/ou Geodésico",
      "value3": false,
      "observation3": "",
      "4": "Tripé(s)",
      "value4": false,
      "observation4": "",
      "5": "Bússola",
      "value5": false,
      "observation5": "",
      "6": "Relógio de pulso",
      "value6": false,
      "observation6": "",
      "7": "Lanterna",
      "value7": false,
      "observation7": "",
      "8": "Cal",
      "value8": false,
      "observation8": "(Marcação de pontos de controle)",
      "9": "Kit limpeza gerals",
      "value9": false,
      "observation9": "",
    };
    _flightChecklist.add(_flightCategory13);

    _flightCategory14["title"] = "Registros";
    _flightCategory14["onComplete"] = false;
    _flightCategory14["editable"] = true;
    _flightCategory14["description"] =
        "Todas as informações que você recolher no campo precisam ser muito bem registradas para que nenhuma delas se perca e possa fazer falta posteriormente. Portanto, atente-se aos itens de registro.";
    _flightCategory14["numQuestions"] = 5;
    _flightCategory14["questions"] = {
      "1": "Prancheta",
      "value1": false,
      "observation1": "",
      "2": "Caneta/Lápis/Apontador",
      "value2": false,
      "observation2": "",
      "3": "Etiquetas/Fita adesiva",
      "value3": false,
      "observation3": "",
      "4": "Notebook/Tablet",
      "value4": false,
      "observation4": "(e seus respectivos cabos)",
      "5": "Câmera fotográfica/Filmadora",
      "value5": false,
      "observation5": "",
    };
    _flightChecklist.add(_flightCategory14);

    _flightCategory15["title"] = "Registros";
    _flightCategory15["onComplete"] = false;
    _flightCategory15["editable"] = true;
    _flightCategory15["description"] =
        "Todas as informações que você recolher no campo precisam ser muito bem registradas para que nenhuma delas se perca e possa fazer falta posteriormente. Portanto, atente-se aos itens de registro.";
    _flightCategory15["numQuestions"] = 5;
    _flightCategory15["questions"] = {
      "1": "Prancheta",
      "value1": false,
      "observation1": "",
      "2": "Caneta/Lápis/Apontador",
      "value2": false,
      "observation2": "",
      "3": "Etiquetas/Fita adesiva",
      "value3": false,
      "observation3": "",
      "4": "Notebook/Tablet",
      "value4": false,
      "observation4": "(e seus respectivos cabos)",
      "5": "Câmera fotográfica/Filmadora",
      "value5": false,
      "observation5": "",
    };
    _flightChecklist.add(_flightCategory15);

    _flightCategory16["title"] = "Alimentação";
    _flightCategory16["onComplete"] = false;
    _flightCategory16["editable"] = true;
    _flightCategory16["description"] =
        "No campo é muito importante ter sempre a mão algum alimento.\nEsquece-lo pode fazer com que você se sinta fraco e menos disposto para o trabalho. Boa alimentação e hidratação são fundamentais!";
    _flightCategory16["numQuestions"] = 2;
    _flightCategory16["questions"] = {
      "1": "Lanche/ Alimentos leves",
      "value1": false,
      "observation1": "",
      "2": "Almoço (marmita) ou dinheiro para a refeição",
      "value2": false,
      "observation2": "",
    };
    _flightChecklist.add(_flightCategory16);

    _flightCategory17["title"] = "Comunicação";
    _flightCategory17["onComplete"] = false;
    _flightCategory17["editable"] = true;
    _flightCategory17["description"] =
        "É importante que você tenha a disposição meios de comunicação para o caso de ocorrer atrasos ou para o caso de se perder.";
    _flightCategory17["numQuestions"] = 1;
    _flightCategory17["questions"] = {
      "1": "Celular/Rádio/Walkie Talkie",
      "value1": false,
      "observation1": "",
    };

    _flightChecklist.add(_flightCategory17);

    _flightCategory18["title"] = "Outros";
    _flightCategory18["onComplete"] = false;
    _flightCategory18["editable"] = true;
    _flightCategory18["description"] =
        "Confira outros itens importantes que você deve lembrar de levar a campo para trabalhar com ARPs.";
    _flightCategory18["numQuestions"] = 4;
    _flightCategory18["questions"] = {
      "1": "Guarda-sol",
      "value1": false,
      "observation1": "",
      "2": "Papel higiênico",
      "value2": false,
      "observation2": "",
      "3": "Mesa e cadeiras compactas",
      "value3": false,
      "observation3": "",
      "4": "Anemômetro Digital ou biruta simples",
      "value4": false,
      "observation4": "",
    };

    _flightChecklist.add(_flightCategory18);

    _flightCategory19["title"] =
        "Importante!\nAntes de voar não se esqueça de verificar estes itens";
    _flightCategory19["onComplete"] = false;
    _flightCategory19["editable"] = true;
    _flightCategory19["description"] = "Nunca voe as cegas";
    _flightCategory19["numQuestions"] = 5;
    _flightCategory19["questions"] = {
      "1": "Verifique se não existem cabos soltos",
      "value1": false,
      "observation1": "",
      "2": "Confira se as hélices estão bem fixas",
      "value2": false,
      "observation2": "",
      "3": "Cheque também o aperto dos parafusos e porcas",
      "value3": false,
      "observation3": "",
      "4":
          "Confira a qualidade e a intensidade do sinal de GPS para o retorno de emergência",
      "value4": false,
      "observation4": "",
      "5": "Calcule o tempo de voo em relação ao peso embarcado",
      "value5": false,
      "observation5": "",
    };

    _flightChecklist.add(_flightCategory19);

    saveData(_flightChecklist);
  }
}
/* void createChecklist() {

    _flightChecklist
        .add(addMap("Anatel", false, "Legislação Aplicável", false));
    _flightChecklist.add(addMap("Anac", false, "Legislação Aplicável", false));
    _flightChecklist.add(addMap("Decea", false, "Legislação Aplicável", false));
    _flightChecklist.add(addMap("MD", false, "Legislação Aplicável", false));
    _flightChecklist.add(addMap(
        "Comando Polícia Militar", false, "Legislação Aplicável", false));
    saveData(_flightChecklist);
  }*/
