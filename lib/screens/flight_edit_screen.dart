import 'package:flutter/material.dart';
import 'package:fly_my_work/data/flight.dart';
import 'package:fly_my_work/data/flight_helper.dart';
import 'package:fly_my_work/widgets/edit_card.dart';

class FlightEditScreen extends StatefulWidget {
  final PageController _pageController;

  FlightEditScreen(this._pageController);
  @override
  _FlightEditScreenState createState() => _FlightEditScreenState();
}

bool isDialog = true;

class _FlightEditScreenState extends State<FlightEditScreen> {
  List<Flight> _flights = List();
  FlightHelper helper = FlightHelper();
  // bool isDialog = true;

  @override
  void initState() {
    super.initState();
    helper.getAllFlights().then((list) {
      setState(() {
        _flights = list;
      });
    });
    if (isDialog) {
      WidgetsBinding.instance
          .addPostFrameCallback((_) => showAlertDialog(context));
      isDialog = false;
    }

    //showAlertDialog(context);
  }

  @override
  Widget build(BuildContext context) {
    if (_flights.length > 0)
      return ListView.builder(
          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
          itemCount: _flights.length,
          itemBuilder: (context, index) {
            return EditCard(_flights[index], widget._pageController);
          });
    else
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Você ainda não possui voôs : (",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 20,
                    fontWeight: FontWeight.bold)),
          ],
        ),
      );
  }

  showAlertDialog(BuildContext context) {
    Widget botaoContinuar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Continuar",
        style: TextStyle(color: Theme.of(context).primaryColor),
      ),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            elevation: 1.0,
            shape:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
            title: Text("Bem-vindo ao Modo de edição de Voo"),
            content: Text(
                "Aqui você pode editar informações do vôo, e remover categorias/checklist.\n"
                "O ícone de cadiado é exibido em alguns itens para informar"
                " estes são inalteráveis."),
            actions: [botaoContinuar]);
      },
    );
  }
}
