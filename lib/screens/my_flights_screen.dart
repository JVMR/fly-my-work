import 'package:flutter/material.dart';
import 'package:fly_my_work/data/flight.dart';
import 'package:fly_my_work/data/flight_helper.dart';
import 'package:fly_my_work/widgets/flight_card.dart';

class MyFlightsScreen extends StatefulWidget {
  final PageController _pageController;

  MyFlightsScreen(this._pageController);
  @override
  _MyFlightsScreenState createState() => _MyFlightsScreenState();
}

class _MyFlightsScreenState extends State<MyFlightsScreen> {
  List<Flight> _flights = List(); //Lista que armazenará todos voos
  FlightHelper helper = FlightHelper(); //Referência ao banco de dados dos voos
  int contFlights = 0;
  @override
  void initState() {
    super.initState();
    helper.getAllFlights().then((list) {
      setState(() {
        _flights = list;
      });
    });
    setState((){
      for (int i = 0; i < _flights.length; i++) {
      if (_flights[i].status != "Arquivado") contFlights++;
    }
    });
    print("Tamanho $contFlights");
  }

  @override
  Widget build(BuildContext context) {
    if (_flights.length > 0) {
      return ListView.builder(
          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
          itemCount: _flights.length,
          itemBuilder: (context, index) {
            if (_flights[index].status != "Arquivado") {
              return FlightCard(_flights[index], widget._pageController);
            } else {
              return Container();
            }
          });
    } else {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Você ainda não possui voôs : (",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 20,
                    fontWeight: FontWeight.bold)),
            Text(
                "Para adicionar novos voôs toque no botão + no canto inferior da tela",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.grey, fontSize: 20)),
          ],
        ),
      );
    }
  }
}
