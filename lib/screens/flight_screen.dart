import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fly_my_work/data/flight.dart';
import 'package:fly_my_work/data/flight_checklist_data.dart' /*as json*/;
import 'package:fly_my_work/data/flight_helper.dart';
import 'package:fly_my_work/screens/edition_screen.dart';

class FlightScreen extends StatefulWidget {
  final Flight flight;

  FlightScreen(this.flight);

  @override
  _FlightScreenState createState() => _FlightScreenState();
}

class _FlightScreenState extends State<FlightScreen> {
  FlightChecklistData checklistData;
  final helper = FlightHelper();
  List flightChecklist = [];

  @override
  void initState() {
    super.initState();
    checklistData = FlightChecklistData(widget.flight.id);

    checklistData.readData(widget.flight.id).then((list) {
      setState(() {
        flightChecklist = json.decode(list);
      });
    });
    checkCompleteFlight();
  }

  @override
  void dispose() {
    super.dispose();

    checkCompleteFlight();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        centerTitle: true,
        title: Text(
          widget.flight.title,
          style: TextStyle(color: Colors.white),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.mode_edit,
              color: Colors.white70,
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => EditionScreen(widget.flight)));
            },
          ),
        ],
      ),
      body: ListView.builder(
          itemCount: flightChecklist.length,
          padding: EdgeInsets.all(10),
          itemBuilder: (context, index) {
            int contQuestions = flightChecklist[index]["numQuestions"];
            Color colorCheck = flightChecklist[index]["onComplete"]
                ? Theme.of(context).primaryColor
                : Colors.grey;

            //print(cont);
            //if (true) {
            return ExpansionTile(
                onExpansionChanged: (c) {
                  setState(() {
                    checkCompleteCategory(index, contQuestions);
                  });
                },
                leading: Icon(Icons.check_circle, color: colorCheck),
                title: Text(flightChecklist[index]["title"] ?? "Nada"),
                children: <Widget>[
                  Card(
                    //margin: EdgeInsets.all(8),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    color: Colors.grey[500],
                    elevation: 0.0,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        flightChecklist[index]["description"] ?? "Outro Nada",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  for (var i = 1; i <= contQuestions; i++)
                    buildListTile(index, "$i"),
                ]);
            // } else
            // return Container();
          }),
    );
  }

  Widget buildListTile(
    int index,
    String i,
  ) {
    if (flightChecklist[index]["questions"][i] != null) {
      return ListTile(
        leading: Icon(Icons.check_circle_outline,
            color: flightChecklist[index]["questions"]["value$i"]
                ? Theme.of(context).primaryColor
                : Colors.grey),
        title: Text(flightChecklist[index]["questions"][i]),
        subtitle: flightChecklist[index]["questions"]["observation$i"] != null
            ? Text(flightChecklist[index]["questions"]["observation$i"])
            : null,
        onTap: () {
          setState(() {
            flightChecklist[index]["questions"]["value$i"] =
                !flightChecklist[index]["questions"]["value$i"];
            checklistData.saveData(flightChecklist);
          });
        },
      );
    } else
      return Container();
  }

  checkCompleteCategory(int index, int contQuestions) {
    bool complete = true;
    for (int i = 1; i <= contQuestions && complete; i++) {
      if (flightChecklist[index]["questions"]["value$i"] != null &&
          !flightChecklist[index]["questions"]["value$i"]) complete = false;
    }

    flightChecklist[index]["onComplete"] = complete;
    checklistData.saveData(flightChecklist);
  }

  checkCompleteFlight() {
    bool isComplete = true;
    for (int i = 0; i < flightChecklist.length && isComplete; i++) {
      if (flightChecklist[i]["onComplete"] == false) {
        isComplete = false;
        print("entrei aqui");
      }
    }
    if (isComplete) {
      widget.flight.status = "completado";
      helper.updateFlight(widget.flight);
    }
  }
}
