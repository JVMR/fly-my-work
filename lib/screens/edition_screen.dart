import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fly_my_work/data/flight.dart';
import 'package:fly_my_work/data/flight_checklist_data.dart';
import 'package:fly_my_work/data/flight_helper.dart';

class EditionScreen extends StatefulWidget {
  final Flight flight;

  EditionScreen(this.flight);
  @override
  _EditionScreenState createState() => _EditionScreenState();
}

class _EditionScreenState extends State<EditionScreen> {
  FlightChecklistData checklistData;

  List flightChecklist = [];
  TextEditingController _titleController = TextEditingController();
  TextEditingController _localController = TextEditingController();
  TextEditingController _dataController = TextEditingController();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  FlightHelper helper = FlightHelper();

  @override
  void initState() {
    super.initState();
    checklistData = FlightChecklistData(widget.flight.id);
    _titleController.text = widget.flight.title;
    _localController.text = widget.flight.local;
    _dataController.text = widget.flight.data;

    checklistData.readData(widget.flight.id).then((list) {
      setState(() {
        flightChecklist = json.decode(list);
      });
    });
  }

  void disposed() {
    super.dispose();
    _titleController.dispose();
    _localController.dispose();
    _dataController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text(
          widget.flight.title,
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
          backgroundColor: Theme.of(context).primaryColor,
          child: Icon(Icons.save),
          onPressed: () async {
            if (_formKey.currentState.validate()) {
              widget.flight.title = _titleController.text;
              widget.flight.local = _localController.text;
              widget.flight.data = _dataController.text;
              widget.flight.status = "Aberto";
              await helper.updateFlight(widget.flight);
              Navigator.of(context).pop();
            }
          }),
      body: ListView.builder(
          itemCount: flightChecklist.length,
          padding: EdgeInsets.all(10),
          itemBuilder: (context, index) {
            int contQuestions = flightChecklist[index]["numQuestions"];
            switch (index) {
              case 0:
                return buildFormFlight();
              default:
                return buildExpasionTile(index, contQuestions);
            }

            // } else
            // return Container();
          }),
    );
  }

  Widget buildExpasionTile(int index, contQuestions) {
    return ExpansionTile(
        leading: IconButton(
          icon: flightChecklist[index]["editable"]
              ? Icon(Icons.remove_circle_outline)
              : Icon(Icons.lock),
          color: Colors.red,
          onPressed: () {
            setState(() {
              flightChecklist.removeAt(index);
              checklistData.saveData(flightChecklist);
            });
          },
        ),
        title: Text(flightChecklist[index]["title"] ?? "Nada"),
        children: <Widget>[
          Card(
            //margin: EdgeInsets.all(8),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            color: Colors.grey[500],
            elevation: 0.0,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                flightChecklist[index]["description"] ?? "Outro Nada",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          for (var i = 1; i <= contQuestions; i++) buildListTile(index, "$i"),
        ]);
  }

  Widget buildFormFlight() {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(height: 10),
          TextFormField(
            // initialValue: widget.flight.title,
            //autofocus: true,
            controller: _titleController,
            decoration: InputDecoration(
              labelText: "Título do Voo",
              /*border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),*/
            ),
            keyboardType: TextInputType.text,
            validator: (value) {
              if (value.isEmpty)
                return "É necessário que o voo possua um título!";
              else
                return null;
            },
          ),
          SizedBox(height: 20),
          TextFormField(
            // initialValue: widget.flight.local,
            controller: _localController,
            decoration: InputDecoration(
              labelText: "Local",
              /*border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),*/
            ),
            keyboardType: TextInputType.text,
            validator: (value) {
              if (value.isEmpty)
                return "É necessário especificar o local do voo!";
              else
                return null;
            },
          ),
          SizedBox(height: 20),
          TextFormField(
            // initialValue: widget.flight.data,
            controller: _dataController,
            decoration: InputDecoration(
              labelText: "Data",
              /*border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),*/
            ),
            keyboardType: TextInputType.datetime,
            validator: (value) {
              if (value.isEmpty)
                return "É necessário especificar a data do voo!";
              else
                return null;
            },
          ),
          SizedBox(height: 15),
        ],
      ),
    );
  }

  Widget buildListTile(
    int index,
    String i,
  ) {
    //String id = "$index";

    if (flightChecklist[index]["questions"][i] != null) {
      bool isEditable = flightChecklist[index]["editable"];
      IconData icon = isEditable ? Icons.remove : Icons.lock;
      Color color = isEditable ? Colors.red : Colors.grey;
      return ListTile(
        leading: Icon(
          icon,
          color: color,
        ),
        title: Text(flightChecklist[index]["questions"][i]),
        subtitle: flightChecklist[index]["questions"]["observation$i"] != null
            ? Text(flightChecklist[index]["questions"]["observation$i"])
            : null,
        onTap: () {
          //print(index);
          //print("${flightChecklist[index]["questions"].length}");
          if (isEditable) {
            setState(() {
              flightChecklist[index]["questions"].remove(i);
              flightChecklist[index]["questions"].remove("value$i");
              flightChecklist[index]["questions"].remove("observation$i");
              //flightChecklist[index]["numQuestions"]--;
              print("alo");
              if (flightChecklist[index]["numQuestions"] == 0)
                flightChecklist.removeAt(index);

              checklistData.saveData(flightChecklist);
            });
          } else {
            showAlertDialog(context);
          }
        },
      );
    } else {
      return Container();
    }
  }

  showAlertDialog(BuildContext context) {
    Widget botaoContinuar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Continuar",
        style: TextStyle(color: Theme.of(context).primaryColor),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            elevation: 1.0,
            shape:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
            title: Text("Essa categoria é protegida!"),
            content: Text(
                "Não é possível remover perguntas de uma categoria protegidas.\nAs categorias protegidas são indicadas com um cadeado."),
            actions: [botaoContinuar]);
      },
    );
  }
}
