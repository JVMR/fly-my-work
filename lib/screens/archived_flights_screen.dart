import 'package:flutter/material.dart';
import 'package:fly_my_work/data/flight.dart';
import 'package:fly_my_work/data/flight_helper.dart';
import 'package:fly_my_work/widgets/archived_card.dart';

class ArchivedFlightsScreen extends StatefulWidget {
  final PageController _pageController;

  ArchivedFlightsScreen(this._pageController);
  @override
  _ArchivedFlightsScreenState createState() => _ArchivedFlightsScreenState();
}

class _ArchivedFlightsScreenState extends State<ArchivedFlightsScreen> {
  List<Flight> _flights = List(); //Lista que armazenará todos voos
  FlightHelper helper = FlightHelper(); //Referência ao banco de dados dos voos
  int contAchivedFlights = 0;

  @override
  void initState() {
    super.initState();
    helper.getAllFlights().then((list) {
      setState(() {
        _flights = list;
        for (int i = 0; i < _flights.length; i++) {
          if (_flights[i].status == "Arquivado") contAchivedFlights++;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (contAchivedFlights > 0) {
      return ListView.builder(
          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
          itemCount: _flights.length,
          itemBuilder: (context, index) {
            if (_flights[index].status == "Arquivado") {
              return ArchivedCard(_flights[index], widget._pageController);
            } else {
              return Container();
            }
          });
    } else {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Você não possui voôs arquivados :)",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 20,
                    fontWeight: FontWeight.bold)),
          ],
        ),
      );
    }
  }
}
