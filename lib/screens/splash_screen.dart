import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:fly_my_work/screens/home_screen.dart';

class SplashScreen extends StatefulWidget {
  final _pageController = PageController();

  SplashScreen(_pageController);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 10)).then((_) {
      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => HomeScreen(widget._pageController),
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
          child: Container(
        height: 200,
        width: 200,
        child: FlareActor("animations/Drony.flr", animation: "juaozinho"),
      )),
    );
  }
}
