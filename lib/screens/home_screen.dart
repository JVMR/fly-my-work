import 'package:flutter/material.dart';
import 'package:fly_my_work/screens/support_screen.dart';
import 'package:fly_my_work/widgets/creation_button.dart';
import 'package:fly_my_work/widgets/custom_drawer.dart';

import 'archived_flights_screen.dart';
import 'flight_edit_screen.dart';
import 'my_flights_screen.dart';

class HomeScreen extends StatefulWidget {
  final PageController _pageController;

  HomeScreen(this._pageController);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    Color primaryColor = Theme.of(context).primaryColor;

    return PageView(
        controller: widget._pageController,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          //MyFlightsScreen(_pageController),
          Scaffold(
            appBar: AppBar(
              title: Text("Meus Voos",
                  style:
                      TextStyle(color: Colors.white, fontFamily: 'RobotoSlab')),
              centerTitle: true,
              backgroundColor: primaryColor,
            ),
            drawer: CustomDrawer(widget._pageController),
            floatingActionButton: CreationButton(widget._pageController),
            body: MyFlightsScreen(widget._pageController),
          ),
          Scaffold(
            appBar: AppBar(
              title: Text("Voos Arquivados",
                  style: TextStyle(color: Colors.white)),
              centerTitle: true,
              backgroundColor: primaryColor,
            ),
            drawer: CustomDrawer(widget._pageController),
            body: ArchivedFlightsScreen(widget._pageController),
          ),
          Scaffold(
              appBar: AppBar(
                  title: Text("Edição de Voo",
                      style: TextStyle(color: Colors.white)),
                  centerTitle: true,
                  backgroundColor: primaryColor),
              drawer: CustomDrawer(widget._pageController),
              body: FlightEditScreen(widget._pageController)),
          SupportScreen(widget._pageController),
        ]);
  }
}
