import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:fly_my_work/models/user_model.dart';
import 'package:fly_my_work/widgets/custom_drawer.dart';
import 'package:scoped_model/scoped_model.dart';

class SupportScreen extends StatefulWidget {
  final PageController _pageController;

  SupportScreen(this._pageController);
  @override
  _SupportScreenState createState() => _SupportScreenState();
}

class _SupportScreenState extends State<SupportScreen> {
  final _formKey = GlobalKey<FormState>();
  final _emailSubjectController = TextEditingController();
  final _emailBodyController = TextEditingController();
  final _bodyFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
      bool isLoggedIn = model.isLoggedIn();
      return Scaffold(
        floatingActionButton: FloatingActionButton(
          backgroundColor:
              isLoggedIn ? Theme.of(context).primaryColor : Colors.grey,
          child: Icon(Icons.send),
          onPressed: isLoggedIn
              ? () {
                  if (_formKey.currentState.validate())
                    sendEmail(
                        model.userData["email"],
                        _emailSubjectController.text,
                        _emailBodyController.text);
                }
              : null, //Exibir o dialogo
        ),
        appBar: AppBar(
            title: Text("Suporte", style: TextStyle(color: Colors.white)),
            centerTitle: true,
            backgroundColor: Theme.of(context).primaryColor),
        drawer: CustomDrawer(widget._pageController),
        body: Form(
          key: _formKey,
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            children: <Widget>[
              SizedBox(height: 15),
              Center(
                  child: Icon(
                Icons.help_outline,
                color: Theme.of(context).primaryColor,
                size: 100,
              )),
              SizedBox(height: 5),
              Text(
                "Como podemos te ajudar ?",
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              TextFormField(
                autofocus: true,
                controller: _emailSubjectController,
                decoration: InputDecoration(
                  helperText: "Síntese do assunto tratado",
                  //hintText: "Síntese do assunto tratado",
                  labelText: "Assunto",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.5)),
                ),
                keyboardType: TextInputType.text,
                onFieldSubmitted: (text) {
                  return FocusScope.of(context).requestFocus(_bodyFocus);
                },
                validator: (value) {
                  if (value.isEmpty)
                    return "É necessário especificar o assunto";
                  else
                    return null;
                },
              ),
              SizedBox(height: 20),
              TextFormField(
                maxLines: 4,
                focusNode: _bodyFocus,
                controller: _emailBodyController,
                decoration: InputDecoration(
                  labelText: "Descrição",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.5)),
                ),
                keyboardType: TextInputType.text,
                validator: (value) {
                  if (value.isEmpty)
                    return "É necessário que seu email possua um corpo!";
                  else
                    return null;
                },
              ),
            ],
          ),
        ),
      );
    });
  }

  void sendEmail(String emailSender, String subject, String body) async {
    final Email email = Email(
      body: emailSender + "\n" + body,
      subject: subject,
      recipients: ["jvreysoad762@gmail.com"],
      isHTML: false,
    );
    await FlutterEmailSender.send(email);
  }
}
