import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fly_my_work/data/flight.dart';
import 'package:fly_my_work/data/flight_checklist_data.dart';
import 'package:fly_my_work/data/flight_helper.dart';

import 'home_screen.dart';

class CreationScreen extends StatefulWidget {
  final PageController _pageController;

  CreationScreen(this._pageController);

  @override
  _CreationScreenState createState() => _CreationScreenState();
}

class _CreationScreenState extends State<CreationScreen> {
  Flight _newFlight;

  FlightHelper helper;

  FlightChecklistData checklistData;

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController _titleController = TextEditingController();
  TextEditingController _localController = TextEditingController();
  TextEditingController _dataController = TextEditingController();

  final _localFocus = FocusNode();
  final _dataFocus = FocusNode();

  @override
  void initState() {
    super.initState();
    _newFlight = Flight();
    helper = FlightHelper();
  }

  void disposed() {
    super.dispose();
    _titleController.dispose();
    _localController.dispose();
    _dataController.dispose();
    _localFocus.dispose();
    _dataFocus.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Adicionar Novo Voo",
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Theme.of(context).primaryColor,
      ),
      floatingActionButton: FloatingActionButton(
          backgroundColor: Theme.of(context).primaryColor,
          child: Icon(Icons.save),
          onPressed: () async {
            if (_formKey.currentState.validate()) {
              _newFlight.title = _titleController.text;
              _newFlight.local = _localController.text;
              _newFlight.data = _dataController.text;
              _newFlight.status = "Aberto";
              _newFlight = await helper.saveFlight(_newFlight);
              checklistData = FlightChecklistData(_newFlight.id);
              checklistData.create();
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                      builder: (context) => HomeScreen(widget._pageController)),
                  ModalRoute.withName("CreationScreen"));
            }
          }),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.only(right: 10, left: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(height: 30),
                TextFormField(
                  autofocus: true,
                  controller: _titleController,
                  decoration: InputDecoration(
                    labelText: "Título do Voo",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12.5)),
                  ),
                  keyboardType: TextInputType.text,
                  onFieldSubmitted: (text) {
                    return FocusScope.of(context).requestFocus(_localFocus);
                  },
                  validator: (value) {
                    if (value.isEmpty)
                      return "É necessário que o voo possua um título!";
                    else
                      return null;
                  },
                ),
                SizedBox(height: 20),
                TextFormField(
                  focusNode: _localFocus,
                  controller: _localController,
                  decoration: InputDecoration(
                    labelText: "Local",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12.5)),
                  ),
                  keyboardType: TextInputType.text,
                  onFieldSubmitted: (text) {
                    return FocusScope.of(context).requestFocus(_dataFocus);
                  },
                  validator: (value) {
                    if (value.isEmpty)
                      return "É necessário especificar o local do voo!";
                    else
                      return null;
                  },
                ),
                SizedBox(height: 20),
                TextFormField(
                  focusNode: _dataFocus,
                  controller: _dataController,
                  decoration: InputDecoration(
                    labelText: "Data",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12.5)),
                  ),
                  keyboardType: TextInputType.datetime,
                  validator: (value) {
                    if (value.isEmpty)
                      return "É necessário especificar a data do voo!";
                    else
                      return null;
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
