import 'package:flutter/material.dart';
import 'package:fly_my_work/screens/home_screen.dart';
import 'package:fly_my_work/screens/splash_screen.dart';
import 'package:scoped_model/scoped_model.dart';

import 'models/user_model.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final PageController _controller = PageController();
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScopedModel<UserModel>(
      model: UserModel(),
      child: MaterialApp(
        title: 'Fly My Work',
        theme: ThemeData(
          splashColor: Color.fromRGBO(62, 155, 22, 1),
          iconTheme: IconThemeData(color: Colors.white),
          fontFamily: 'RobotoSlab',
          primaryColor: Color.fromRGBO(87, 195, 40, 1), //57C328
          focusColor: Color.fromRGBO(62, 155, 22, 1),
          //verde secundário Color.fromRGBO(62, 155, 22, 1),
        ),
        home: SplashScreen(_controller),
      ),
    );
  }
}
