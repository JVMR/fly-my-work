import 'package:flutter/material.dart';
import 'package:fly_my_work/screens/flight_edit_screen.dart';

class DrawerTile extends StatelessWidget {
  final IconData icon;
  final String text;
  final PageController controller;
  final int page;

  DrawerTile(this.icon, this.text, this.controller, this.page);

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(12.5),
        bottomLeft: Radius.circular(12.5),
      ),
      color: controller.page.round() == page
          ? Color.fromRGBO(255, 255, 255, 0.7)
          : Colors.transparent,
      child: InkWell(
        onTap: () {
          Navigator.of(context).pop();
          //if (page == 2) showAlertDialog(context);

          controller.jumpToPage(page);
        },
        child: Container(
          /*color: controller.page.round() == page
              ? Color.fromRGBO(255, 255, 255, 0.5)
              : Colors.transparent,*/
          height: 60.0,
          child: Row(
            children: <Widget>[
              Icon(
                icon,
                size: 32.0,
                color: controller.page.round() == page
                    ? Theme.of(context).primaryColor
                    : Colors.black,
              ),
              SizedBox(width: 32.0),
              Text(
                text,
                style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: controller.page.round() == page
                      ? FontWeight.bold
                      : FontWeight.normal,
                  color: controller.page.round() == page
                      ? Theme.of(context).primaryColor
                      : Colors.black,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    Widget botaoContinuar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Continuar",
        style: TextStyle(color: Theme.of(context).primaryColor),
      ),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            elevation: 1.0,
            shape:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
            title: Text("Bem-vindo ao Modo de edição de Voo"),
            content: Text(
                "Aqui você pode adicionar e remover categorias/checklist.\n"
                "O ícone de cadiado é exibido em alguns itens para informar"
                " que aquele item não pode ser alterado."),
            actions: [botaoContinuar]);
      },
    );
  }
}
