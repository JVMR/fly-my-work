import 'package:flutter/material.dart';
import 'package:fly_my_work/screens/creation_screen.dart';

class CreationButton extends StatelessWidget {
  final PageController _pageController;
  CreationButton(this._pageController);
  @override
  Widget build(BuildContext context) {
    Color primaryColor = Theme.of(context).primaryColor;
    return FloatingActionButton(
      backgroundColor: primaryColor,
      child: Icon(
        Icons.add,
        color: Colors.white,
      ),
      onPressed: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => CreationScreen(_pageController)));
      },
    );
  }
}
