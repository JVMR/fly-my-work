import 'package:flutter/material.dart';
import 'package:fly_my_work/data/flight.dart';
import 'package:fly_my_work/data/flight_helper.dart';
import 'package:fly_my_work/screens/archived_flights_screen.dart';
import 'package:fly_my_work/screens/home_screen.dart';

class ArchivedCard extends StatelessWidget {
  final PageController _pageController;
  final Flight flight;

  final FlightHelper helper = FlightHelper();

  ArchivedCard(this.flight, this._pageController);

  @override
  Widget build(BuildContext context) {
    Color colorCheck = flight.status == "completado"
        ? Theme.of(context).primaryColor
        : Colors.grey[500];
    return InkWell(
      onTap: () {
        return showAlertDialog(context);
      },
      child: Card(
        color: Colors.grey[300],
        elevation: 5.0,
        shape: OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
        margin: EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Icon(
                    Icons.check_circle,
                    color: colorCheck,
                  ),
                  Expanded(
                      child: Text(
                    flight.title,
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  )),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
              child: Text(flight.data),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
              child: Text(flight.local),
            ),
          ],
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    Widget botaoDesarquivar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Desarquivar",
        style: TextStyle(color: Theme.of(context).primaryColor),
      ),
      onPressed: () {
        flight.status = "Aberto";
        helper.updateFlight(flight);
        Navigator.pop(context);
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (context) => HomeScreen(_pageController)),
            ModalRoute.withName("HomeScreen"));
      },
    );
    Widget botaoApagar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Apagar",
        style: TextStyle(color: Colors.redAccent),
      ),
      onPressed: () {
        helper.deleteFlight(flight.id);
        //state.setState(() {});
        //Navigator.pop(context);
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (context) => HomeScreen(_pageController)),
            ModalRoute.withName("HomeScreen"));
      },
    );
    Widget botaoCancelar = FlatButton(
      child: Text(
        "Cancelar",
        style: TextStyle(color: Colors.grey),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            elevation: 1.0,
            shape:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
            title: Text(flight.title),
            content:
                Text("O que você deseja fazer com o voo '${flight.title}' ?"),
            actions: [botaoApagar, botaoCancelar, botaoDesarquivar]);
      },
    );
  }
}
