import 'package:flutter/material.dart';
import 'package:fly_my_work/data/flight.dart';
import 'package:fly_my_work/data/flight_helper.dart';
import 'package:fly_my_work/screens/edition_screen.dart';

class EditCard extends StatelessWidget {
  final PageController _pageController;
  final Flight flight;

  final FlightHelper helper = FlightHelper();

  EditCard(this.flight, this._pageController);

  @override
  Widget build(BuildContext context) {
    /*Color colorCheck = flight.status == "completado"
        ? Theme.of(context).primaryColor
        : Colors.grey[500];*/
    return InkWell(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => EditionScreen(flight)));
      },
      child: Card(
        elevation: 5.0,
        shape: OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
        margin: EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Icon(
                    Icons.mode_edit,
                    color: Colors.red,
                  ),
                  Expanded(
                      child: Text(
                    flight.title,
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  )),
                  /*IconButton(
                    icon: Icon(Icons.more_vert),
                    color: Theme.of(context).primaryColor,
                    onPressed: () {},
                  ),*/
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 0.0, 8, 5),
              child: Text(flight.data),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
              child: Text(flight.local),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
              child: Text(flight.status,
                  style: TextStyle(
                    fontStyle: FontStyle.italic,
                    color: flight.status == "aberto"
                        ? Theme.of(context).primaryColor
                        : Colors.grey,
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
