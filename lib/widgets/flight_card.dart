import 'package:flutter/material.dart';
import 'package:fly_my_work/data/flight.dart';
import 'package:fly_my_work/data/flight_helper.dart';
import 'package:fly_my_work/screens/flight_screen.dart';
import 'package:fly_my_work/screens/home_screen.dart';

class FlightCard extends StatelessWidget {
  final PageController _pageController;
  final Flight flight;

  final FlightHelper helper = FlightHelper();

  FlightCard(this.flight, this._pageController);

  @override
  Widget build(BuildContext context) {
    Color colorCheck = flight.status == "completado"
        ? Theme.of(context).primaryColor
        : Colors.grey[500];
    return InkWell(
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => FlightScreen(flight)));
        },
        child: Container(
          child: Card(
            elevation: 5.0,
            borderOnForeground: false,
            shape: OutlineInputBorder(
              borderRadius: BorderRadius.circular(12.5),
            ),
            margin: EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Icon(
                        Icons.check_circle,
                        color: colorCheck,
                      ),
                      Expanded(
                          child: Text(
                        flight.title,
                        style: TextStyle(
                            fontSize: 16,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      )),
                      IconButton(
                        icon: Icon(Icons.more_vert),
                        color: Theme.of(context).primaryColor,
                        onPressed: () {
                          return showAlertDialog(context);
                        },
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 0.0, 8, 5),
                  child: Text(flight.data),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 3, 8, 10),
                  child: Text(flight.local),
                ),
              ],
            ),
          ),
        ));
  }

  showAlertDialog(BuildContext context) {
    Widget botaoArquivar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Arquivar",
        style: TextStyle(color: Colors.blueAccent),
      ),
      onPressed: () {
        flight.status = "Arquivado";
        helper.updateFlight(flight);
        Navigator.pop(context);
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (context) => HomeScreen(_pageController)),
            ModalRoute.withName("HomeScreen"));
      },
    );
    Widget botaoApagar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Apagar",
        style: TextStyle(color: Colors.redAccent),
      ),
      onPressed: () {
        helper.deleteFlight(flight.id);
        //state.setState(() {});
        //Navigator.pop(context);
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (context) => HomeScreen(_pageController)),
            ModalRoute.withName("HomeScreen"));
      },
    );
    Widget botaoCancelar = FlatButton(
      child: Text(
        "Cancelar",
        style: TextStyle(color: Colors.grey),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            elevation: 1.0,
            shape:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
            title: Text(flight.title),
            content:
                Text("O que você deseja fazer com o voo '${flight.title}' ?"),
            actions: [botaoApagar, botaoArquivar, botaoCancelar]);
      },
    );
  }
}
